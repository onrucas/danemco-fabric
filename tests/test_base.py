#!/usr/bin/env python
from __future__ import print_function
import sys
import unittest
import subprocess


class BaseTest(unittest.TestCase):

	def test_list(self):
		subprocess.check_call(['fab', '--list', '-f', 'tests/fabfile.py'])
