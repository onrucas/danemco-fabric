from __future__ import print_function
from fabric.api import *

from .base import lrun, exists


def git_root(is_local=False):

    if is_local or 'local' in env.roles:
        return local("git rev-parse --show-toplevel", capture=True)

    require('conf')
    with cd(env.conf.path):
        print("path is", env.conf.path)
        return run("git rev-parse --show-toplevel")


@task
@runs_once
def push():
    """Send your local changes up."""
    local("git push")


@task
def changes():
    """Output the number of new commits"""
    with cd(env.conf.path):
        lrun("git fetch")
        changes = int(lrun("git rev-list HEAD..@{u} --count", capture=True))
        print("%s changes to apply" % changes)
        return changes


@task
def clone():
    repo = local('git config --get remote.origin.url', capture=True)
    lrun('git clone %s %s' % (repo, env.conf.path))


@task
def pull():
    """Update the repository."""
    require("conf")
    if not exists(env.conf.path):
        execute('vc.clone', host=env.host)
    with cd(env.conf.path):
        with settings(warn_only=True):
            result = lrun("git diff --quiet HEAD && git diff --cached --quiet HEAD")
        if result.failed:
            abort("There are uncommitted changes on %s" % env.roles)
        lrun('git checkout %s' % env.conf.branch)
        lrun("git pull")

