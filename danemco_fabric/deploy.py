import os
import six
import sys
from distutils.version import LooseVersion

from fabric.api import *
from fabric.contrib.files import exists

from .base import lrun, settings_flag, venv


@task
def upgrade():
    """Deploy and upgrade the packages based on the requirements file"""

    require("conf")
    execute('vc.pull', host=env.host)
    execute('build.virtualenv', host=env.host)
    execute('build.requirements', host=env.host, update=True)
    execute('deploy.migrate', host=env.host)
    execute('deploy.staticfiles', host=env.host)
    execute('deploy.restart', host=env.host)
    if 'true' in getattr(env.conf, 'deploy-cronjobs', 'true').lower():
        execute('deploy.cronjobs', host=env.host)


@task
@runs_once
def staticfiles():
    """Collect Static files"""

    require("conf")

    execute("deploy.npm")
    execute("deploy.bower")

    with settings(warn_only=True):
        if env.host in ['localhost', '127.0.0.1']:
            execute('deploy.command', cmd="collectstatic --noinput -l -v0", host=env.host)
        else:
            execute('deploy.command', cmd="collectstatic --noinput -v0", host=env.host)


@task
@runs_once
def bower():
    install_util("bower.json", "bower install --config.interactive=false", "bower")


@task
def npm():
    install_util("package.json", "npm install", "npm")


def install_util(filename, cmd, check_setting=None):
    """ if the given file exists, run the command """
    require("conf")

    if env.host in ['localhost', '127.0.0.1']:
        if os.path.exists(filename):
            lrun(cmd)
    else:
        if check_setting and env.conf[check_setting]:
            with cd(env.conf.path):
                with settings(warn_only=True):
                    if exists(filename):
                        lrun(cmd)


@task
@runs_once
def migrate():
    """Migrate the Database"""

    require("conf")
    with cd(env.conf.path):
        with venv():
            dj_version = lrun('python -c "import django;print(django.get_version())"', capture=True)
            if LooseVersion(dj_version) >= LooseVersion("1.7.0"):
                execute('deploy.command', cmd="migrate --noinput -v0", host=env.host)
            else:
                execute('deploy.command', cmd="syncdb --noinput -v0", host=env.host)
                with settings(warn_only=True):
                    execute('deploy.command', cmd="migrate --noinput -v0 --all", host=env.host)


@task
def restart():
    """Restart the website"""
    require("conf")

    if getattr(env.conf, 'clear_caches', None):
        execute('deploy.clearcache', host=env.host)

    if hasattr(env.conf, "restart_cmd"):
        with cd(env.conf.path):
            lrun(env.conf.restart_cmd)
    elif env.conf.name:
        lrun("sudo /etc/init.d/django-sites restart %s" % env.conf.name)


@task
@runs_once
def cronjobs():
    require('conf')
    with cd(env.conf.path):
        with venv():
            with settings(warn_only=True):
                lrun('python manage.py installtasks %s -v0' % settings_flag())


@task
@runs_once
def clearcache(key=None):
    """
    Clear configured django cache backends based on site.conf.

    Two options for clearing cache.
    # site.conf - simple option #1; will just clear default cache

    clear_caches = True
    ...

    # site.conf - specific option #2; will clear each cache key

    clear_caches = default,my-custom-cache,my-second-custom-cache
    """
    require("conf")

    with settings(warn_only=True):
        caches = getattr(env.conf, 'clear_caches', None)
        keys = []

        # manual call to refresh cache
        if key and isinstance(key, six.string_types):
            caches = key

        if caches and isinstance(caches, six.string_types):

            if 'true' in caches.lower():
                keys = ['default']

            else:
                keys = [
                    k.strip()
                    for k in caches.strip().strip(',').split(',')
                    if k != ''
                ]

            execute(
                'deploy.command', cmd="clear_cache {} -v0".format(' '.join(keys)),
                host=env.host
            )
        else:
            sys.stdout.write('No actions taken to clear caches.\n')


@task
def command(cmd):
    with cd(env.conf.path):
        with venv():
            settings = settings_flag()
            if "--settings=" in cmd:
                settings = ""
            lrun('python manage.py {cmd} {settings}'.format(
                cmd=cmd,
                settings=settings,
            ))


@task(default=True)
def deploy(force=False):
    """Run the update, migrate, staticfiles and restart tasks"""

    require("conf")
    with settings(conf=env.sections.local, warn_only=True):
        if 'true' in getattr(env.conf, 'vc-push', 'true').lower():
            execute('vc.push', role="local", host=env.host)
    if force:
        hosts_with_changes = True
    else:
        changes = execute('vc.changes', host=env.host)
        hosts_with_changes = [h for h, v in changes.items() if v]
    if hosts_with_changes:
        execute('vc.pull', host=env.host)
        execute('build.virtualenv', host=env.host)
        execute('build.requirements', host=env.host)
        execute('deploy.migrate', host=env.host)
        execute('deploy.staticfiles', host=env.host)
        execute('deploy.restart', host=env.host)
        if 'true' in getattr(env.conf, 'deploy-cronjobs', 'true').lower():
            execute('deploy.cronjobs', host=env.host)
