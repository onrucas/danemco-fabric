import os

from fabric.operations import sudo, local
from fabric.decorators import task

try:
    from danemco_fabric import *
    configure()
except ImportError:

    print "Failed to locate danemco-fabric, which is required for most operations"
    print
    print "    pip install -e git+ssh://git@git.velocitywebworks.com:4000/lib/danemco-fabric.git#egg=danemco_fabric"

    exit(1)
