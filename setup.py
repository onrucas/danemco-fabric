#!/usr/bin/env python
from setuptools import setup, find_packages

exec(open('danemco_fabric/version.py').read())
setup(
    name='danemco_fabric',
    version=__version__,
    description="A collection of fabric utils",
    author="Danemco, LLC",
    author_email='dev@velocitywebworks.com',
    url='https://git.velocitywebworks.com/lib/danemco-fabric.git',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['fabric>=1.7.0', 'requests', 'python-gitlab', 'six'],
)
